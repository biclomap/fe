const ROUTES = {
  APP: 'app',
  MAP: 'map',
  TRACKS: 'TRACKS',
};

export const ROUTE_APP = `/${ROUTES.APP}`;

export const ROUTE_MAP = `/${ROUTES.APP}/${ROUTES.MAP}`;

export const ROUTE_TRACKS = `/${ROUTES.APP}/${ROUTES.TRACKS}`;

export default ROUTES;
